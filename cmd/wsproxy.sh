rename_function cmd_revproxy orig_cmd_revproxy
cmd_revproxy() {
    orig_cmd_revproxy "$@"
    [[ $1 == 'add' ]] &&  _customize_apache2_config
}

_customize_apache2_config() {
    # change apache2 configuration
    # see also: https://nextcloud.com/collaboraonline/
    cat <<EOF > $CONTAINERS/revproxy/domains/$DOMAIN.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        ProxyPass /.well-known/acme-challenge !
        ProxyPass / http://$CONTAINER:9980/
        ProxyPassReverse / http://$CONTAINER:9980/
        ProxyRequests off
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN
        ProxyPreserveHost On

        SSLEngine on
        SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key
        #SSLCertificateChainFile /etc/ssl/certs/ssl-cert-snakeoil.pem

        SSLProxyEngine on
        SSLProxyVerify none
        SSLProxyCheckPeerCN off
        SSLProxyCheckPeerName off
        SSLProxyCheckPeerExpire off

        # Encoded slashes need to be allowed
        AllowEncodedSlashes NoDecode

        # static html, js, images, etc. served from loolwsd
        # loleaflet is the client part of LibreOffice Online
        ProxyPass           /loleaflet https://$CONTAINER:9980/loleaflet retry=0
        ProxyPassReverse    /loleaflet https://$CONTAINER:9980/loleaflet

        # WOPI discovery URL
        ProxyPass           /hosting/discovery https://$CONTAINER:9980/hosting/discovery retry=0
        ProxyPassReverse    /hosting/discovery https://$CONTAINER:9980/hosting/discovery

        # Main websocket
        ProxyPassMatch "/lool/(.*)/ws$" wss://$CONTAINER:9980/lool/$1/ws nocanon

        # Admin Console websocket
        ProxyPass   /lool/adminws wss://$CONTAINER:9980/lool/adminws

        # Download as, Fullscreen presentation and Image upload operations
        ProxyPass           /lool https://$CONTAINER:9980/lool
        ProxyPassReverse    /lool https://$CONTAINER:9980/lool

        # Endpoint with information about availability of various features
        ProxyPass           /hosting/capabilities https://$CONTAINER:9980/hosting/capabilities retry=0
        ProxyPassReverse    /hosting/capabilities https://$CONTAINER:9980/hosting/capabilities

        ProxyPass / https://$CONTAINER:9980/
        ProxyPassReverse / https://$CONTAINER:9980/
        ProxyRequests off
</VirtualHost>
EOF
    # reload the new configuration
    ds @revproxy reload
}
