APP=collabora
IMAGE=collabora/code
DOMAIN="collabora.example.org"

### Domains of the nextcloud servers.
NC_DOMAINS='cloud\\.example\\.org'
#NC_DOMAINS='cloud\\.example\\.org\|cloud\\.example\\.com'
