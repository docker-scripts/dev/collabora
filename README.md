# Collabora Online

See: https://nextcloud.com/collaboraonline/
and: https://www.collaboraoffice.com/code/docker/

## Installation

  - First install `ds` and `revproxy`:
      - https://gitlab.com/docker-scripts/ds#installation
      - https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull collabora`

  - Create a directory for the container: `ds init collabora @collabora`

  - Fix the settings: `cd /var/ds/collabora/ ; vim settings.sh`

  - Create the container: `ds create`
  
## Update

```bash
ds remove
ds create
```

## Other commands

```
ds stop
ds start
ds shell
ds help
```
